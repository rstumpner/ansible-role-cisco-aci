import json
import pytest


@pytest.mark.parametrize('postman_collection', ['../../tests/build/ansible-role-cisco-aci/localhost-ansible-role-cisco-aci-postman.json'])
def test_postman_collection_info(postman_collection):
    with open(postman_collection) as f:
        change = json.load(f)
    rule = change['info']
    assert len(rule['name']) >= 0  , "Postman Collection name should be defined"
#def test_postman_collection_apic_login(postman_collection):
#    with open(postman_collection) as f:
#        change = json.load(f)
#    rule = change['item']
#    assert rule['name'] == "ACI Basic login", "Policy name should be ACI Basic login"

#    assert rule['name'] == *, "Policy is not Postman"