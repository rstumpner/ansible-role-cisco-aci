# Ansible Role for Cisco ACI Configuration

This is a Ansible Role Cisco ACI Configuration. The Role is tested against a CI/CD Pipeline in Gitlab and have some Tags on the Tasks to Impelement some capabilities for a devops style deployment. 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-cisco.aci
```
## Features
#### Build the ACI Configuration in JSON
Build the ACI Configuration in pure JSON to copy paste it to Postman (https://www.postman.com/) or VS Code Thunderclient (https://www.thunderclient.io/)

###### Step by Step Guide

###### ACI Features
- Tenant
- Bridgedomains

#### Build the ACI Configuration as Postman Collection
Build the ACI Configuration as Postman Collection to execute the intendet configuration with Postman (https://www.postman.com/) or VS Code Thunderclient (https://www.thunderclient.io/)

###### Step by Step Guide

###### ACI Features
- Tenant

#### Build the ACI CLI
Build the ACI Configuration for the ACI CLI avaliable on the APIC Controler.
###### Step by Step Guide

###### ACI Features
- Tenant

#### Deploy the ACI Configuration via Ansible
Deploy the ACI Configuration for the ACI Fabric via Ansible.
###### Step by Step Guide

###### ACI Features
- Tenant

Tested:
 - Vagrant Ubuntu 18.04
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at